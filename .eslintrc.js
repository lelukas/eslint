module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/recommended',
    'eslint:all'
  ],
  'plugins': [
    "sort-keys-fix",
    "no-null"
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "vue/require-v-for-key": "off",
    "vue/html-self-closing": "error",
    "vue/require-default-prop": "off",
    "vue/mustache-interpolation-spacing": [
      2,
      "always"
    ],
    "vue/no-spaces-around-equal-signs-in-attribute": "error",
    "vue/max-attributes-per-line": [
      2,
      {
        "singleline": 4,
        "multiline": {
          "max": 1,
          "allowFirstLine": true
        }
      }
    ],
    "vue/order-in-components": [
      "error",
      {
        "order": [
          "el",
          "name",
          "functional",
          [
            "directives",
            "filters"
          ],
          "parent",
          "components",
          "mixins",
          "extends",
          "model",
          "data",
          [
            "props",
            "propsData"
          ],
          "inheritAttrs",
          "computed",
          "methods",
          "watch",
          "beforeCreate",
          "created",
          "beforeMount",
          "mounted",
          "beforeUpdate",
          "updated",
          "beforeDestroy",
          "destroyed",
          [
            "template",
            "render"
          ],
          [
            "delimiters",
            "comments"
          ],
          "renderError"
        ]
      }
    ],
    "vue/attributes-order": "error",
    "yoda": "warn",
    "sort-keys": "off",
    "sort-keys-fix/sort-keys-fix": ["error", "asc", {"caseSensitive": true, "natural": false}],
    "no-undefined": "off",
    "no-null/no-null": 2,
    "semi": "off",
    "quote-props": "off",
    "quotes": ["error", "single", { "allowTemplateLiterals": true }],
    "indent": ["error", 2],
    "max-len": ["error", {
      "ignorePattern": "<((?=!\\-\\-)!\\-\\-[\\s\\S]*\\-\\-|((?=\\?)\\?[\\s\\S]*\\?|((?=\\/)\\/[^.\\-\\d][^\\/\\]'\"[!#$%&()*+,;<=>?@^`{|}~ ]*|[^.\\-\\d][^\\/\\]'\"[!#$%&()*+,;<=>?@^`{|}~ ]*(?:\\s[^.\\-\\d][^\\/\\]'\"[!#$%&()*+,;<=>?@^`{|}~ ]*(?:=(?:\"[^\"]*\"|'[^']*'|[^'\"<\\s]*))?)*)\\s?\\/?))>"
    }]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
